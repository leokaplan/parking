import serial
from xbee import ZigBee

tty = '/dev/ttyUSB0'
baud_rate = 9600

serial_port = serial.Serial(tty,baud_rate)
zb = ZigBee(serial_port)    
print "starting"
while True:
    try:
        print "waiting"
        data = zb.wait_read_frame()
        print "received:"
        print data
    except KeyboardInterrupt:
        break

serial_port.close()
